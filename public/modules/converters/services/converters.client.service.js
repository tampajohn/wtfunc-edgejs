'use strict';

//Converters service used to communicate Converters REST endpoints
angular.module('converters').factory('Converters', ['$resource',
	function($resource) {
		return $resource('converters/:converterId', { converterId: '@_id'
		}, {
            convert: {
                method: 'POST'
            },
            list: {
                method: 'GET',
                isArray: false
            }
		});
	}
]);