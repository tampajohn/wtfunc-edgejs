'use strict';

// Converters controller
angular.module('converters').controller('ConvertersController', ['$scope', '$stateParams', '$location', 'Authentication', 'Converters',
	function($scope, $stateParams, $location, Authentication, Converters ) {
		$scope.authentication = Authentication;

		// Find a list of Converters
		$scope.find = function() {
			$scope.response = Converters.list();
		};

        $scope.convert = function() {
            Converters.convert({color: $scope.color, type: $scope.type}, function(result) {
                $scope.response = result;
            });
        };

        $scope.delete = function() {
            Converters.delete(function(result) {
                $scope.response = result;
            });
        };
	}
]);