'use strict';

(function() {
	// Converters Controller Spec
	describe('Converters Controller Tests', function() {
		// Initialize global variables
		var ConvertersController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Converters controller.
			ConvertersController = $controller('ConvertersController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Converter object fetched from XHR', inject(function(Converters) {
			// Create sample Converter using the Converters service
			var sampleConverter = new Converters({
				name: 'New Converter'
			});

			// Create a sample Converters array that includes the new Converter
			var sampleConverters = [sampleConverter];

			// Set GET response
			$httpBackend.expectGET('converters').respond(sampleConverters);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.converters).toEqualData(sampleConverters);
		}));

		it('$scope.findOne() should create an array with one Converter object fetched from XHR using a converterId URL parameter', inject(function(Converters) {
			// Define a sample Converter object
			var sampleConverter = new Converters({
				name: 'New Converter'
			});

			// Set the URL parameter
			$stateParams.converterId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/converters\/([0-9a-fA-F]{24})$/).respond(sampleConverter);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.converter).toEqualData(sampleConverter);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Converters) {
			// Create a sample Converter object
			var sampleConverterPostData = new Converters({
				name: 'New Converter'
			});

			// Create a sample Converter response
			var sampleConverterResponse = new Converters({
				_id: '525cf20451979dea2c000001',
				name: 'New Converter'
			});

			// Fixture mock form input values
			scope.name = 'New Converter';

			// Set POST response
			$httpBackend.expectPOST('converters', sampleConverterPostData).respond(sampleConverterResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Converter was created
			expect($location.path()).toBe('/converters/' + sampleConverterResponse._id);
		}));

		it('$scope.update() should update a valid Converter', inject(function(Converters) {
			// Define a sample Converter put data
			var sampleConverterPutData = new Converters({
				_id: '525cf20451979dea2c000001',
				name: 'New Converter'
			});

			// Mock Converter in scope
			scope.converter = sampleConverterPutData;

			// Set PUT response
			$httpBackend.expectPUT(/converters\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/converters/' + sampleConverterPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid converterId and remove the Converter from the scope', inject(function(Converters) {
			// Create new Converter object
			var sampleConverter = new Converters({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Converters array and include the Converter
			scope.converters = [sampleConverter];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/converters\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleConverter);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.converters.length).toBe(0);
		}));
	});
}());