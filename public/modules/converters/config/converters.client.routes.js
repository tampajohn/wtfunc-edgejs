'use strict';

//Setting up route
angular.module('converters').config(['$stateProvider',
	function($stateProvider) {
		// Converters state routing
		$stateProvider.
		state('listConverters', {
			url: '/converters',
			templateUrl: 'modules/converters/views/list-converters.client.view.html'
		});
	}
]);