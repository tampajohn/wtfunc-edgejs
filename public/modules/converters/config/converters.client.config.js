'use strict';

// Configuring the Articles module
angular.module('converters').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Converters', 'converters', 'converters');

	}
]);