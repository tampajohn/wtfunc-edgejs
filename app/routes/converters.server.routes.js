'use strict';

module.exports = function(app) {
	var converters = require('../../app/controllers/converters');

	// Converters Routes
	app.route('/converters')
		.get(converters.read)
		.post(converters.convert)
        .delete(converters.delete);
};