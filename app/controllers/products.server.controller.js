'use strict';

/**
 * Module dependencies.
 */
var edge = require('edge'),
    config = require('../../config/config'),
    _ = require('lodash');

/**
 * Create a Product using an MSSQL stored proc
 */
exports.create = function(req, res) {
    var createProduct = edge.func('sql', {
        source: 'exec CreateProduct',
        connectionString: config.sql.connectionString
    });
	var product = req.body;
	product.CreatedBy = req.user.username;
    product.CreatedOn = Date.now();

    createProduct(product, function(error, result) {
        if (error) throw error;
        if (result.length === 0) throw 'Couldn\'t save product ' + product.Id;
        res.jsonp(result[0]);
    });
};

/**
 * Show the current Product
 */
exports.read = function(req, res) {
	res.jsonp(req.product);
};

/**
 * Update a Product using SQL query and passing json object from client
 */
exports.update = function(req, res) {
	var product = req.product ;

	product = _.extend(product , req.body);
    product.UpdatedBy = req.user.username;
    product.UpdatedOn = Date.now();

    var updateProduct = edge.func('sql', {
        source: 'update products set Name = @Name, Sku = @Sku, UpdatedBy = @UpdatedBy, UpdatedOn = @UpdatedOn where Id = @Id',
        connectionString: config.sql.connectionString
    });

    console.log(product);
    updateProduct(product, function(error, result) {
        if (error) throw error;
        if (result.length === 0) throw 'couldn\'t save product ' + product.Id;
        res.jsonp(result[0]);
    });
};

/**
 * Delete an Product using SQL query
 */
exports.delete = function(req, res) {
    var deleteProduct = edge.func('sql', {
        source: 'delete products where id = @id',
        connectionString: config.sql.connectionString
    });
	var product = req.product;
    deleteProduct({id:product.Id}, function(error, result) {
        if (error) throw error;
        res.jsonp(product);
    });
};

/**
 * List of Products
 */
exports.list = function(req, res) {
    var getProducts = edge.func('sql', {
        source: 'select * from products',
        connectionString: config.sql.connectionString
    });

    getProducts(null, function(error, result) {
        if (error) throw error;
        console.log(result);
        res.jsonp(result);
    });
};

/**
 * Product middleware
 */
exports.productByID = function(req, res, next, id) {
    var getProductById = edge.func('sql', {
        source: 'select * from products where Id = @id',
        connectionString: config.sql.connectionString
    });
    getProductById({id: id}, function(error, result) {
        if (error || result.length === 0) return next(new Error('Failed to load Product ' + id));
        req.product = result[0];
        next();
    });
};

/**
 * Product authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	next();
};