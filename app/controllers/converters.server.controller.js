'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors'),
    fs = require('fs'),
    path = require('path'),
    edge = require('edge'),
	_ = require('lodash'),
    dirPath = '.\\public\\processed\\',
    options = [
        'jpg',
        'bmp',
        'png',
        'gif'
    ],
    colors = [
        '#FFFFFF',
        '#000000',
        '#DADADA',
        '#00FF00',
        '#FF0000',
        '#0000FF',
        '#DDCCBB',
        '#123456',
        '#556677',
        '#aa00ff'
    ]


/**
 * Create a Converter
 */


var getFiles = function(req, res) {
    fs.readdir('.\\public\\processed', function(error, files) {
        var response = {
            options: options,
            colors: colors,
            results: _.map(files, function(value) { return '/processed/' + value; })
        };
        res.jsonp(response);
    });
};

exports.convert = function(req, res) {
    var convertImage = edge.func(function() {/*
    #r "System.Drawing.dll"

    using System;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;

    class Startup
    {
        static IDictionary<string, ImageFormat> formats
               = new Dictionary<string, ImageFormat>
            {
                { "jpg", ImageFormat.Jpeg },
                { "bmp", ImageFormat.Bmp },
                { "gif", ImageFormat.Gif },
                { "tiff", ImageFormat.Tiff },
                { "png", ImageFormat.Png }
            };

        public async Task<object> Invoke(
                  IDictionary<string,object> input)
        {
            await Task.Run(async () => {

                using (Image image = Image.FromFile((string)input["source"]))
                {
                     using(Bitmap b = new Bitmap(image.Width, image.Height)) {
                        b.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                        using(Graphics g = Graphics.FromImage(b)) {
                            g.Clear(ColorTranslator.FromHtml((string)input["color"]));
                            g.DrawImageUnscaled(image, 0, 0);
                        }
                        b.Save((string)input["destination"], formats[(string)input["toType"]]);
                     }

                }
            });
            return null;
        }
    }*/});
    var params = {
        source: '.\\public\\modules\\core\\img\\brand\\brand.png',
        destination: dirPath + 'brand.' + req.body.color.substring(1) + req.body.type,
        toType: req.body.type,
        color: req.body.color
    };

    convertImage(params, function (error, result) {
        if (error) throw error;
        console.log('The image shiju.png has been asynchronously converted');
        getFiles(req, res);
    });
};


exports.read = getFiles;

exports.delete = function(req, res) {
    fs.readdir(dirPath, function(error, files) {
        _.each(files, function(file) {
            var filePath = dirPath + file;
                fs.unlink(filePath);
        });
        var response = {
            options: options,
            colors: colors,
            results: []
        };
        res.jsonp(response);
    });
};