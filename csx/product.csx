public class Product {
	public string Name { get;set; }
	public string Sku { get;set; }
	public string CreatedBy { get; set; }
	public int CreatedOn { get; set; }
	public string UpdatedBy { get; set; }
	public int UpdatedOn { get; set; }
}