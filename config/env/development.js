'use strict';

module.exports = {
	db: 'mongodb://wtfunc:wtfunc@ds051160.mongolab.com:51160/wtfunc-edgejs',
	app: {
		title: 'WTFunc Edge.js - Development Environment'
	},
    sql: {
        connectionString: process.env.EDGE_SQL_CONNECTION_STRING || 'Data Source=localhost;Initial Catalog=WTFunc;Integrated Security=True'
    },
	facebook: {
		clientID: process.env.FACEBOOK_ID || '405035182983261',
		clientSecret: process.env.FACEBOOK_SECRET || 'cd66c8f7616302f5e4049c4d66c5dd61',
		callbackURL: 'http://localhost:3000/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'NgqQCZ7qbhwIYPIzdljPbiEzu',
		clientSecret: process.env.TWITTER_SECRET || 'NNe9Uc3Yd0n2Hr1N9D0en5c1ZXyfjJOgjcBhvBUEkNiA3x7tBF',
		callbackURL: 'http://localhost:3000/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || '670305518095-jpss48315gb0qeo4plm71oinclpjgcjc.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || '1AjIBYbaUiW43VjEHBF7EyAd',
		callbackURL: 'http://localhost:3000/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || '75gn72pc7wc9gc',
		clientSecret: process.env.LINKEDIN_SECRET || 'JHEQxvsN93aJnmSp',
		callbackURL: 'http://localhost:3000/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || '865fd4b39ab59f73c39b',
		clientSecret: process.env.GITHUB_SECRET || 'e332fca23cbf3daeddc3463733b2e4a5e576f780',
		callbackURL: 'http://localhost:3000/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	}
};